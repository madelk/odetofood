﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OdeToFood.Entities;
using OdeToFood.Services;
using OdeToFood.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OdeToFood.Controllers
{
    public class HomeController : Controller
    {
        private IRestaurantData _restaurantData;
        private IGreeter _greeter;

        public HomeController(IRestaurantData restaurantData, IGreeter greeter){
            _restaurantData = restaurantData;
            _greeter = greeter;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            var model = new HomePageViewModel();
            model.Restraunts = _restaurantData.GetAll();
            model.CurrentMessage = _greeter.GetGreeting();
            return View(model);
		}

        public IActionResult Details(int id)
        {
            var model = _restaurantData.Get(id);
            if (model == null) {
                return RedirectToAction(nameof(Index));
            }
            return View(model);
		}

		[HttpGet]
		public IActionResult Create()
		{
			return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(RestaurantEditViewModel model)
        {
            if (!ModelState.IsValid) {
                return View();
            }
            var newRestaurant = new Restaurant {
                Cuisine = model.Cuisine,
                Name = model.Name
            };
            newRestaurant = _restaurantData.Add(newRestaurant);

            return RedirectToAction("Details", new { id = newRestaurant.Id });
        }
    }
}
