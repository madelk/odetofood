﻿using System.ComponentModel.DataAnnotations;

namespace OdeToFood.ViewModels
{
    public class RestaurantEditViewModel
	{
        [Required, MaxLength(80)]
		public string Name { get; set; }
		public Entities.CuisineType Cuisine { get; set; }
    }
}   
